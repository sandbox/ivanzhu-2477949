<?php

/**
 * @param $node_type
 *
 * @return bool
 */
function node_workflow_get_wid($node_type) {
	$node_workflow_query = db_select('node_workflow', 'nw')
		->fields('nw')
		->condition('nw.node_type', $node_type, '=');
	$node_workflow_result = $node_workflow_query->execute()->fetchAssoc();
	return isset($node_workflow_result['wid']) ? $node_workflow_result['wid'] : FALSE;
}

function node_workflow_get_enabled_types() {
	$node_workflow_query = db_select('node_workflow', 'nw')
		->fields('nw', array('node_type'))
		->condition('nw.status', 1, '=');
	$node_workflow_result = $node_workflow_query->execute()->fetchCol(0);
//	dpm($node_workflow_result);
	return $node_workflow_result;
}

/**
 * @param $wid
 *
 * @return mixed
 */
function node_workflow_get_root_step_by_wid($wid) {


	$node_workflow_row_query = db_select('node_workflow_row', 'nwr')
		->fields('nwr')
		->condition('nwr.pid', '0', '=')
		->condition('nwr.wid', $wid, '=');
	$node_workflow_row_result = $node_workflow_row_query->execute()->fetchAssoc();
	return $node_workflow_row_result;
}

/**
 * @param $id step id
 *
 * @return mixed
 */
function node_workflow_get_row_by_id($id) {
	$node_workflow_row_query = db_select('node_workflow_row', 'nwr')
		->fields('nwr')
		->condition('nwr.id', $id, '=');
	$node_workflow_row_result = $node_workflow_row_query->execute()->fetchAssoc();
	return $node_workflow_row_result;
}

function node_workflow_get_step_info($nid, $sid) {
	$query = db_select('node_workflow_item', 'nwi');
	$query->join('node_workflow_node_item', 'nwni', 'nwi.id = nwni.item_id');
	$query->fields('nwi');
	$query->condition('nwni.nid', $nid, '=');
	$query->orderBy('nwi.id', 'DESC');
	$result = $query->execute()->fetchAssoc();
	$node_workflow_rows = unserialize($result['data']);
	$step = array();
	foreach ($node_workflow_rows as $row) {
		if (in_array($row['type'], array(
				'initial_step',
				'step',
				'ending_step'
			)) && $row['id'] == $sid
		) {
			$step = $row;
		}
	}
	return $step;
}

function node_workflow_get_current_step_by_nid($nid) {
	$node = node_load($nid);
	$current_node_workflow_history = end($node->node_workflow->data);
	$step = node_workflow_get_step_info($node->nid, $current_node_workflow_history['sid']);
	return $step;
//	$query_step = db_select('node_workflow_data', 'nwd')
//		->fields('nwd', array('sid'))
//		->condition('nwd.nid', $nid, '=')
//		->orderBy('nwd.id','DESC')
//		->execute()->fetchAssoc();
//
//
//	$query = db_select('node_workflow_item', 'nwi');
//	$query->join('node_workflow_node_item', 'nwni', 'nwi.id = nwni.item_id');
//	$query->fields('nwi');
//	$query->conditions('nwni.nid', $nid, '=');
//	$query->orderBy('nwi.id', 'DESC');
//	$result = $query->execute()->fetchAssoc();
//	$node_workflow_rows = unserialize($result['data']);
//
//
//	$steps = array();
//	foreach($node_workflow_rows as $row) {
//		if($row['type'] == 'step') {
//			$steps[] = $row;
//		}
//	}
//	return end($steps);
}


function node_workflow_enabled_check($node_type) {
	$query_node_workflow = db_select('node_workflow', 'nw')
		->fields('nw')
		->condition('nw.node_type', $node_type->type, '=');

	$result_node_workflow = $query_node_workflow->execute()->fetchAll();
	$workflow_info = empty($result_node_workflow) ? array() : $result_node_workflow[0];
	if (isset($workflow_info->status) && $workflow_info->status) {
		return TRUE;
	}
	return FALSE;
}

/**
 * include three types of step (initial step, step, ending step)
 *
 * @param $nid
 *
 * @return array
 */
function node_workflow_get_all_steps_by_nid($nid, $step_types) {
	$query = db_select('node_workflow_item', 'nwi');
	$query->join('node_workflow_node_item', 'nwni', 'nwi.id = nwni.item_id');
	$query->fields('nwi');
	$query->conditions('nwni.nid', $nid, '=');
	$query->orderBy('nwi.id', 'DESC');
	$result = $query->execute()->fetchAssoc();
	$node_workflow_rows = unserialize($result['data']);
	$steps = array();
	foreach ($node_workflow_rows as $row) {
		if (in_array($row['type'], $step_types)) {
			$steps[] = $row;
		}
	}
	return $steps;
}

function node_workflow_get_all_steps($step_types) {
	$query = db_select('node_workflow_item', 'nwi');
	$query->fields('nwi');
	$query->conditions('nwi.status', 1, '=');
	$results = $query->execute()->fetchAllAssoc('id', PDO::FETCH_ASSOC);
	$steps = array();
	foreach ($results as $result) {
		$node_workflow_rows = unserialize($result['data']);

		foreach ($node_workflow_rows as $row) {
			if (in_array($row['type'], $step_types) && !in_array($row, $steps)) {
				$steps[] = $row;
			}
		}
	}
	return $steps;
}


function node_workflow_get_all_actions_of_step($nid, $sid) {
	$query = db_select('node_workflow_item', 'nwi');
	$query->Join('node_workflow_node_item', 'nwni', 'nwi.id = nwni.item_id');
	$query->fields('nwi');
	$query->condition('nwni.nid', $nid, '=');
	$result = $query->execute()->fetchAssoc();
	$node_workflow_rows = unserialize($result['data']);
	$actions = array();
	foreach ($node_workflow_rows as $row) {
		if ($row['type'] == 'action' && $row['pid'] == $sid) {
			$actions[] = $row;
		}
	}
	return $actions;
}

/**
 *
 * @return bool
 */
function node_workflow_conditional_func_default() {
	return TRUE;
}


function node_workflow_row_get_data($node_type, $pid = 0) {
	$node_workflow_query = db_select('node_workflow', 'nw')
		->fields('nw')
		->condition('nw.node_type', $node_type, '=');
	$node_workflow_result = $node_workflow_query->execute()->fetchAssoc();
	$wid = $node_workflow_result['wid'];

	$root_rows = db_query('SELECT * FROM {node_workflow_row} WHERE (pid = :pid AND wid = :wid ) ORDER BY weight ASC', array(
		':pid' => $pid,
		':wid' => $wid
	));

	$itemtree = array();
	$depth = -1;
	foreach ($root_rows as $parent_row) {
		node_workflow_row_get_tree($wid, $parent_row, $itemtree, $depth);
	}
	return $itemtree;

}

function node_workflow_row_get_tree($wid, $parentitem, &$itemtree = array(), &$depth = 0) {
	$depth++;
	$parentitem->depth = $depth;
	$parent_row_info = (array) $parentitem;

//	$parent_step_info['type'] = 'step';
	$itemtree[$parentitem->id] = $parent_row_info;
	$children = db_query('SELECT *
                      FROM {node_workflow_row}
                      WHERE (pid = :pid and wid = :wid )
                      ORDER BY weight ASC',
		array(':pid' => $parentitem->id, ':wid' => $wid));
	foreach ($children as $child) {
		// Make sure this child does not already exist in the tree, to avoid loops.
		if (!in_array($child->id, array_keys($itemtree))) {
			// Add this child's tree to the $itemtree array.
			node_workflow_row_get_tree($wid, $child, $itemtree, $depth);
		}
	}
	// Finished processing this tree branch.  Decrease our $depth value by one
	// to represent moving to the next branch.
	$depth--;
}

/**
 *
 * Now only support og role name
 *
 * @param $user current user
 * @param $roles array('employee', 'manager')
 *
 * @return bool
 */
function node_workflow_user_role_check($user, $roles) {
	$roles[] = 'member';
	$gids = og_get_groups_by_user($user, 'node');
	foreach ($gids as $gid) {
		$node = node_load($gid);
		if ($node->type == 'department') {
			$og_roles = og_get_user_roles('node', $gid, $user->uid);
			sort($roles);
			sort($og_roles);
			if ($roles == $og_roles) {
				return TRUE;
			}
		}
	}
	return FALSE;
}

function node_workflow_get_emails($node) {
	$emails = array();

	$node = node_load($node['#node']->nid);
	$node_workflow_data = $node->node_workflow->data;
	$current = end($node_workflow_data);
	$current_step_id = $current['sid'];
	$step_info = node_workflow_get_step_info($node->nid, $current_step_id);
	if ($step_info['options']['type'] == 'step') {
		$permission = $step_info['options']['permission'];
		if (isset($step_info['options']['permission_purview']) && $step_info['options']['permission_purview']) {
			$target_id = $node->og_group_ref['und'][0]['target_id'];
			if (isset($permission[$target_id])) {
				foreach ($permission[$target_id] as $rid => $settings) {
					if ($settings['settings']['update']) {
						$uids = og_extras_get_users_by_roles($target_id, array($rid), 'node');
						foreach ($uids as $uid) {
							$group_user = user_load($uid->uid);
							$language = language_default();
							$emails[] = array(
								'to' => $group_user->mail,
								'language' => $language,
								'params' => array(
									'message' => t("A request needs your review"),
									'review_link' => url('node/' . $node->nid . '/edit', array('absolute' => TRUE)),
								),
								'from' => variable_get('site_mail', 'ivan@tipnote.com'),
								'send' => TRUE,
							);
						}
					}
				}
			}
		}
		else {
			foreach ($permission as $key_item => $item) {
				foreach ($item as $rid => $settings) {
					if ($settings['settings']['update']) {
						$uids = og_extras_get_users_by_roles($key_item, array($rid), 'node');
						foreach ($uids as $uid) {
							$group_user = user_load($uid->uid);
							$language = language_default();
							$emails[] = array(
								'to' => $group_user->mail,
								'language' => $language,
								'params' => array(
									'message' => t("A request needs your review"),
									'review_link' => url('node/' . $node->nid . '/edit', array('absolute' => TRUE)),
								),
								'from' => variable_get('site_mail', 'ivan@tipnote.com'),
								'send' => TRUE,
							);
						}
					}
				}
			}
		}
	}
	elseif ($step_info['options']['type'] == 'ending_step') {
		$permission = $step_info['options']['permission'];
		if (isset($step_info['options']['permission_purview']) && $step_info['options']['permission_purview']) {
			$target_id = $node->og_group_ref['und'][0]['target_id'];
			if (isset($permission[$target_id])) {
				foreach ($permission[$target_id] as $rid => $settings) {
					if ($settings['settings']['view']) {
						$uids = og_extras_get_users_by_roles($target_id, array($rid), 'node');
						foreach ($uids as $uid) {
							$group_user = user_load($uid->uid);
							$language = language_default();
							$emails[] = array(
								'to' => $group_user->mail,
								'language' => $language,
								'params' => array(
									'message' => t("A request needs your review"),
									'review_link' => url('node/' . $node->nid . '/edit', array('absolute' => TRUE)),
								),
								'from' => variable_get('site_mail', 'ivan@tipnote.com'),
								'send' => TRUE,
							);
						}
					}
				}
			}
		}
		else {
			foreach ($permission as $key_item => $item) {
				foreach ($item as $rid => $settings) {
					if ($settings['settings']['view']) {
						$uids = og_extras_get_users_by_roles($key_item, array($rid), 'node');
						foreach ($uids as $uid) {
							$group_user = user_load($uid->uid);
							$language = language_default();
							$emails[] = array(
								'to' => $group_user->mail,
								'language' => $language,
								'params' => array(
									'message' => t("A request needs your review"),
									'review_link' => url('node/' . $node->nid . '/edit', array('absolute' => TRUE)),
								),
								'from' => variable_get('site_mail', 'ivan@tipnote.com'),
								'send' => TRUE,
							);
						}
					}
				}
			}
		}
	}

	return $emails;
}

function node_workflow_get_parent_action($nid, $sid) {
	$step = node_workflow_get_step_info($nid, $sid);
	$query = db_select('node_workflow_item', 'nwi');
	$query->join('node_workflow_node_item', 'nwni', 'nwi.id = nwni.item_id');
	$query->fields('nwi');
	$query->condition('nwni.nid', $nid, '=');
	$result = $query->execute()->fetchAssoc();
	$node_workflow_rows = unserialize($result['data']);
//	$actions = array();
	foreach ($node_workflow_rows as $row) {
		if ($row['type'] == 'action' && $row['id'] == $step['pid']) {
			return $row;
		}
	}
	return FALSE;
}

function node_workflow_node_step_index_info($node) {
	$step_info = '';
	if (isset($node->node_workflow)) {
		$data = $node->node_workflow->data;
		$current_step_index = count($data);
		$step_info .= $current_step_index;

	}

	return $step_info;
}


function node_workflow_node_step_info($node) {
	$step_info = '';
	if (isset($node->node_workflow)) {
		$data = $node->node_workflow->data;
		$end = end($data);
		$step = node_workflow_get_step_info($node->nid, $end['sid']);
		if ($step['type'] == 'ending_step') {
			$step_info = t('Finished');
		}
		else {
			$step_info = t('In process');
		}

	}

	return $step_info;
}


function _node_workflow_node_children_step($step, $rows, $step_sum = NULL) {
	$current_step = $step;
	foreach ($rows as $key => $row) {
		if ($row['depth'] <= $current_step['depth']) {
			unset($rows[$key]);
		}
	}

	foreach ($rows as $key => $row) {
		if ($row['pid'] == $step['id']) {

		}
	}

}

function node_workflow_node_last_action($node) {
	$info = '';
	if (isset($node->node_workflow)) {
		$data = $node->node_workflow->data;
		end($data);
		if(count($data) == 1) {
			$history = end($data);
		} else {
			$history = prev($data);
		}
		if (prev($data)) {
			$action = node_workflow_get_parent_action($node->nid, $history['sid']);
			if ($action) {
				$user = user_load($history['data']['uid']);
				$info = $action['options']['action_button'] . ' By ' . l($user->name, 'user/' . $user->uid);
			}
		}
		else {
			$user = user_load($history['data']['uid']);
			$info = t('Create') . ' By ' . l($user->name, 'user/' . $user->uid);
		}
	}

	return $info;
}
