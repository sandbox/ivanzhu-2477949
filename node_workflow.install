<?php

function node_workflow_schema() {
	$schema['node_workflow'] = array(
		'description' => 'Stores workflow options of specify node type.',
		'fields' => array(
			'wid' => array(
				'type' => 'serial',
				'not null' => TRUE,
				'description' => 'The primary identifier for a workflow.',
			),
			'node_type' => array(
				'type' => 'varchar',
				'length' => 32,
				'not null' => TRUE,
				'default' => '',
				'description' => 'The machine name of node type.',
			),
			'status' => array(
				'description' => 'Boolean indicating whether the workflow is enabled (visible to non-administrators).',
				'type' => 'int',
				'not null' => TRUE,
				'default' => 1,
			),
		),
		'primary key' => array('wid', 'node_type'),
	);

	$schema['node_workflow_row'] = array(
		'description' => 'Stores latest workflow options of specify node type.',
		'fields' => array(
			'id' => array(
				'type' => 'serial',
				'not null' => TRUE,
				'description' => 'The primary identifier for a workflow.',
			),
			'wid' => array(
				'type' => 'int',
				'not null' => TRUE,
				'description' => 'The primary identifier for a workflow.',
			),
			'name' => array(
				'type' => 'varchar',
				'length' => 32,
				'not null' => TRUE,
				'default' => '',
				'description' => 'The machine name of node type.',
			),
			'machine_name' => array(
				'type' => 'varchar',
				'length' => 32,
				'not null' => TRUE,
				'default' => '',
				'description' => 'The machine name of node type.',
			),
			'type' => array(
				'type' => 'varchar',
				'length' => 32,
				'not null' => TRUE,
			),
			'description' => array(
				'description' => 'A brief description of this row.',
				'type' => 'text',
				'not null' => TRUE,
				'size' => 'medium',
			),
			'options' => array(
				'type' => 'blob',
				'size' => 'big',
				'not null' => TRUE,
				'serialize' => TRUE,
				'description' => 'Serialized data containing the options for the row.',
			),
			'weight' => array(
				'description' => 'The sortable weight for this item',
				'type' => 'int',
				'length' => 11,
				'not null' => TRUE,
				'default' => 0,
			),
			'pid' => array(
				'description' => 'The primary id of the parent for this item',
				'type' => 'int',
				'length' => 11,
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'depth' => array(
				'description' => 'The depth of this item within the tree',
				'type' => 'int',
				'size' => 'small',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
		),
		'primary key' => array('id'),
	);

	$schema['node_workflow_item'] = array(
		'description' => 'Store workflow data for each reversion.',
		'fields' => array(
			'id' => array(
				'type' => 'serial',
				'not null' => TRUE,
				'description' => 'The automatic index for this table.'
			),
			'wid' => array(
				'type' => 'int',
				'not null' => TRUE,
				'description' => ''
			),
			'status' => array(
				'description' => 'Boolean indicating whether the workflow is used',
				'type' => 'int',
				'not null' => TRUE,
				'default' => 0,
			),
			'data' => array(
				'type' => 'blob',
				'size' => 'big',
				'not null' => TRUE,
				'serialize' => TRUE,
				'description' => 'Serialized data.',
			),
		),
		'primary key' => array('id'),
	);

	$schema['node_workflow_node_item'] = array(
		'description' => 'Store workflow data for each node.',
		'fields' => array(
			'id' => array(
				'type' => 'serial',
				'not null' => TRUE,
				'description' => 'The automatic index for this table.'
			),
			'nid' => array(
				'type' => 'int',
				'not null' => TRUE,
				'description' => ''
			),
			'item_id' => array(
				'type' => 'int',
				'not null' => TRUE,
				'description' => 'The foreign key of node workflow item.'
			),

		),
		'primary key' => array('id'),
	);

	$schema['node_workflow_data'] = array(
		'description' => 'Stores workflow data for node.',
		'fields' => array(
			'id' => array(
				'type' => 'serial',
				'not null' => TRUE,
				'description' => 'The auto index for this table.',
			),
			'wid' => array(
				'type' => 'int',
				'not null' => TRUE,
				'description' => 'The primary identifier of a workflow.',
			),
			'nid' => array(
				'type' => 'int',
				'not null' => TRUE,
				'description' => 'node id.',
			),
			'sid' => array(
				'type' => 'int',
				'not null' => TRUE,
				'description' => 'The step id of workflow.',
			),
			'data' => array(
				'type' => 'blob',
				'size' => 'big',
				'not null' => TRUE,
				'serialize' => TRUE,
				'description' => 'Serialized data.',
			),
			'created' => array(
				'description' => 'The Unix timestamp when the node was created.',
				'type' => 'int',
				'not null' => TRUE,
				'default' => 0,
			),
		),
		'primary key' => array('id', 'nid'),
	);

	return $schema;
}