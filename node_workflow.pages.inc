<?php

/**
 * @param $form
 * @param $form_state
 * @param $node_type
 *
 * @return mixed
 */
function node_workflow_form($form, &$form_state, $node_type) {
	$wid = node_workflow_get_wid($node_type->type);

	$form['wid'] = array(
		'#type' => 'hidden',
		'#value' => $wid,
	);
//	$form['node_type'] = array(
//		'#type' => 'hidden',
//		'#value' => $node_type->type,
//	);

	$form['node_workflow_rows']['#tree'] = TRUE;
	$rows = node_workflow_row_get_data($node_type->type);
	foreach ($rows as $item) {
		// Each entry will be an array using the the unique id for that item as
		// the array key, and an array of table row data as the value.
		$form['node_workflow_rows'][$item['id']] = array(
			'type' => array(
				'#markup' => $item['type'],
			),
			// We'll use a form element of type '#markup' to display the item name.
			'name' => array(
				'#markup' => $item['name'],
			),
			// We'll use a form element of type '#textfield' to display the item
			// description, to demonstrate that form elements can be included in the
			// table. We limit the input to 255 characters, which is the limit we
			// set on the database field.
			'description' => array(
				'#markup' => $item['description'],
			),
			// For parent/child relationships, we also need to add form items to
			// store the current item's unique id and parent item's unique id.
			//
			// We would normally use a hidden element for this, but for this example
			// we'll use a disabled textfield element called 'id' so that we can
			// display the current item's id in the table.
			//
			// Because tabledrag modifies the #value of this element, we use
			// '#default_value' instead of '#value' when defining a hidden element.
			// Also, because tabledrag modifies '#value', we cannot use a markup
			// element, which does not support the '#value' property. (Markup
			// elements use the '#markup' property instead.)
			'id' => array(
				// '#type' => 'hidden',
				// '#default_value' => $item->id,
				'#type' => 'textfield',
				'#size' => 3,
				'#default_value' => $item['id'],
				'#disabled' => TRUE,
			),
			// The same information holds true for the parent id field as for the
			// item id field, described above.
			'pid' => array(
				// '#type' => 'hidden',
				// '#default_value' => $item->pid,
				'#type' => 'textfield',
				'#size' => 3,
				'#default_value' => $item['pid'],
			),
			// The 'weight' field will be manipulated as we move the items around in
			// the table using the tabledrag activity.  We use the 'weight' element
			// defined in Drupal's Form API.
			'weight' => array(
				'#type' => 'weight',
				'#title' => t('Weight'),
				'#default_value' => $item['weight'],
				'#delta' => 10,
				'#title_display' => 'invisible',
			),
			// We'll use a hidden form element to pass the current 'depth' of each
			// item within our parent/child tree structure to the theme function.
			// This will be used to calculate the initial amount of indentation to
			// add before displaying any child item rows.
			'depth' => array(
				'#type' => 'hidden',
				'#value' => $item['depth'],
			),
		);

		switch ($item['type']) {
			case 'step':
				$variables = array();
				$links = array();
				// add action link
				$links['action_add']['title'] = t('Add action');
				$links['action_add']['href'] = 'admin/structure/types/manage/' . $node_type->type . '/node_workflow/action/add';
				//step edit link
				$links['step_edit']['title'] = t('Edit');
				$links['step_edit']['href'] = 'admin/structure/types/manage/' . $node_type->type . '/node_workflow/step/' . $item['id'] . '/edit';
				//step delete link
				$links['step_delete']['title'] = t('Delete');
				$links['step_delete']['href'] = 'admin/structure/types/manage/' . $node_type->type . '/node_workflow/step/' . $item['id'] . '/delete';
				$variables['links'] = $links;
				$form['node_workflow_rows'][$item['id']]['operations'] = array(
					'#markup' => theme('links__node_workflow_operations', $variables),
				);
				break;
			case 'initial_step':
				$variables = array();
				$links = array();
				// add action link
				$links['action_add']['title'] = t('Add action');
				$links['action_add']['href'] = 'admin/structure/types/manage/' . $node_type->type . '/node_workflow/action/add';
				//initial step edit link
				$links['step_edit']['title'] = t('Edit');
				$links['step_edit']['href'] = 'admin/structure/types/manage/' . $node_type->type . '/node_workflow/initial_step/' . $item['id'] . '/edit';

				$variables['links'] = $links;
				$form['node_workflow_rows'][$item['id']]['operations'] = array(
					'#markup' => theme('links__node_workflow_operations', $variables),
				);
				break;
			case 'ending_step':
				$variables = array();
				$links = array();

				//step edit link
				$links['step_edit']['title'] = t('Edit');
				$links['step_edit']['href'] = 'admin/structure/types/manage/' . $node_type->type . '/node_workflow/ending_step/' . $item['id'] . '/edit';
				//step delete link
				$links['step_delete']['title'] = t('Delete');
				$links['step_delete']['href'] = 'admin/structure/types/manage/' . $node_type->type . '/node_workflow/ending_step/' . $item['id'] . '/delete';
				$variables['links'] = $links;
				$form['node_workflow_rows'][$item['id']]['operations'] = array(
					'#markup' => theme('links__node_workflow_operations', $variables),
				);
				break;
			case 'action':
				$variables = array();
				$links = array();
				// add step link
				$links['step_add']['title'] = t('Add step');
				$links['step_add']['href'] = 'admin/structure/types/manage/' . $node_type->type . '/node_workflow/step/add';
				//step edit link
				$links['action_edit']['title'] = t('Edit');
				$links['action_edit']['href'] = 'admin/structure/types/manage/' . $node_type->type . '/node_workflow/action/' . $item['id'] . '/edit';
				//step delete link
				$links['action_delete']['title'] = t('Delete');
				$links['action_delete']['href'] = 'admin/structure/types/manage/' . $node_type->type . '/node_workflow/action/' . $item['id'] . '/delete';
				$variables['links'] = $links;
				$form['node_workflow_rows'][$item['id']]['operations'] = array(
					'#markup' => theme('links__node_workflow_operations', $variables),
				);
				break;
		}

	}

	// Now we add our submit button, for submitting the form results.
	//
	// The 'actions' wrapper used here isn't strictly necessary for tabledrag,
	// but is included as a Form API recommended practice.
	$form['actions'] = array('#type' => 'actions');
	$form['actions']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save Changes')
	);

	return $form;
}

/**
 * Theme callback for the node_workflow_form form.
 */
function theme_node_workflow_form($variables) {
	$form = $variables['form'];

	// Initialize the variable which will store our table rows.
	$rows = array();
	// Iterate over each element in our $form['example_items'] array.
	foreach (element_children($form['node_workflow_rows']) as $id) {

		// Before we add our 'weight' column to the row, we need to give the
		// element a custom class so that it can be identified in the
		// drupal_add_tabledrag call.
		//
		// This could also have been done during the form declaration by adding
		// @code
		//   '#attributes' => array('class' => 'example-item-weight'),
		// @endcode
		// directly to the 'weight' element in tabledrag_example_simple_form().
		$form['node_workflow_rows'][$id]['weight']['#attributes']['class'] = array('node-workflow-item-weight');

		// In the parent/child example, we must also set this same custom class on
		// our id and parent_id columns (which could also have been done within
		// the form declaration, as above).
		$form['node_workflow_rows'][$id]['id']['#attributes']['class'] = array('node-workflow-item-id');
		$form['node_workflow_rows'][$id]['pid']['#attributes']['class'] = array('node-workflow-item-pid');
		$form['node_workflow_rows'][$id]['operations']['#attributes']['class'] = array('node-workflow-item-operations');

		// To support the tabledrag behaviour, we need to assign each row of the
		// table a class attribute of 'draggable'. This will add the 'draggable'
		// class to the <tr> element for that row when the final table is
		// rendered.
		$class = array('draggable');

		// We can add the 'tabledrag-root' class to a row in order to indicate
		// that the row may not be nested under a parent row.  In our sample data
		// for this example, the description for the item with id '8' flags it as
		// a 'root' item which should not be nested.
//		if ($form['node_workflow_rows'][$id]['pid']['#value'] == '0') {
//			$class[] = 'tabledrag-root';
//		}

		// We can add the 'tabledrag-leaf' class to a row in order to indicate
		// that the row may not contain child rows.  In our sample data for this
		// example, the description for the item with id '9' flags it as a 'leaf'
		// item which can not contain child items.
//		if ($id == '9') {
//			$class[] = 'tabledrag-leaf';
//		}

		// If this is a child element, we need to add some indentation to the row,
		// so that it appears nested under its parent.  Our $depth parameter was
		// calculated while building the tree in tabledrag_example_parent_get_data
		$indent = theme('indentation', array('size' => $form['node_workflow_rows'][$id]['depth']['#value']));
//		unset($form['node_workflow_rows'][$id]['depth']);
		dpm($form['node_workflow_rows'][$id]['depth']);
		// We are now ready to add each element of our $form data to the $rows
		// array, so that they end up as individual table cells when rendered
		// in the final table.  We run each element through the drupal_render()
		// function to generate the final html markup for that element.
		$rows[] = array(
			'data' => array(
				// Add our 'name' column, being sure to include our indentation.
				$indent . drupal_render($form['node_workflow_rows'][$id]['name']),
				//
				drupal_render($form['node_workflow_rows'][$id]['type']),
				// Add our 'description' column.
				drupal_render($form['node_workflow_rows'][$id]['description']),
				// Add our 'weight' column.
				drupal_render($form['node_workflow_rows'][$id]['weight']),
				// Add our hidden 'id' column.
				drupal_render($form['node_workflow_rows'][$id]['id']),
				// Add our hidden 'parent id' column.
				drupal_render($form['node_workflow_rows'][$id]['pid']),
				drupal_render($form['node_workflow_rows'][$id]['operations'])
			),
			// To support the tabledrag behaviour, we need to assign each row of the
			// table a class attribute of 'draggable'. This will add the 'draggable'
			// class to the <tr> element for that row when the final table is
			// rendered.
			'class' => $class,
		);
	}


	$rows[] = array(
		'data' => array(
			l(t('Add step'), 'admin/structure/types/manage/'. arg(4).'/node_workflow/step/add') . '&nbsp;&nbsp;&nbsp;' .
			l(t('Add initial step'), 'admin/structure/types/manage/'. arg(4).'/node_workflow/initial_step/add') . '&nbsp;&nbsp;&nbsp;' .
			l(t('Add ending step'), 'admin/structure/types/manage/'. arg(4).'/node_workflow/ending_step/add'),
		),
	);

	// We now define the table header values.  Ensure that the 'header' count
	// matches the final column count for your table.
	//
	// Normally, we would hide the headers on our hidden columns, but we are
	// leaving them visible in this example.
	// $header = array(t('Name'), t('Description'), '', '', '');
	$header = array(
		t('Name'),
		t('Type'),
		t('Description'),
		t('Weight'),
		t('ID'),
		t('PID'),
		t('Operations')
	);

	// We also need to pass the drupal_add_tabledrag() function an id which will
	// be used to identify the <table> element containing our tabledrag form.
	// Because an element's 'id' should be unique on a page, make sure the value
	// you select is NOT the same as the form ID used in your form declaration.
	$table_id = 'node-workflow-row-items-table';

	// We can render our tabledrag table for output.
	$output = theme('table', array(
		'header' => $header,
		'rows' => $rows,
		'attributes' => array('id' => $table_id),
	));

	// And then render any remaining form elements (such as our submit button).
	$output .= drupal_render_children($form);

	// We now call the drupal_add_tabledrag() function in order to add the
	// tabledrag.js goodness onto our page.
	//
	// For our parent/child tree table, we need to pass it:
	// - the $table_id of our <table> element (example-items-table),
	// - the $action to be performed on our form items ('match'),
	// - a string describing where $action should be applied ('parent'),
	// - the $group value (pid column) class name ('example-item-pid'),
	// - the $subgroup value (pid column) class name ('example-item-pid'),
	// - the $source value (id column) class name ('example-item-id'),
	// - an optional $hidden flag identifying if the columns should be hidden,
	// - an optional $limit parameter to control the max parenting depth.
	drupal_add_tabledrag($table_id, 'match', 'parent', 'node-workflow-item-pid', 'node-workflow-item-pid', 'node-workflow-item-id', FALSE);

	// Because we also want to sort in addition to providing parenting, we call
	// the drupal_add_tabledrag function again, instructing it to update the
	// weight field as items at the same level are re-ordered.
	drupal_add_tabledrag($table_id, 'order', 'sibling', 'node-workflow-item-weight', NULL, NULL, FALSE);

	return $output;
}

/**
 * Submit callback for the node_workflow_form form.
 */
function node_workflow_form_submit($form, &$form_state) {
	// update node workflow info in node workflow row table.
	foreach ($form_state['values']['node_workflow_rows'] as $id => $item) {
		db_query(
			"UPDATE {node_workflow_row} SET weight = :weight, pid = :pid, depth = :depth WHERE id = :id",
			array(':weight' => $item['weight'], ':pid' => $item['pid'], ':depth' => $item['depth'], ':id' => $id)
		);
	}
	// store workflow configuration into node workflow item table.
	$ids = array_keys($form_state['values']['node_workflow_rows']);
	$workflow_conf = db_select('node_workflow_row', 'nwr')
			->fields('nwr')
			->condition('nwr.id', $ids , 'IN')
			->execute()->fetchAllAssoc('id', PDO::FETCH_ASSOC);
	foreach($workflow_conf as &$value) {
		$value['options'] = unserialize($value['options']);
	}
	dpm($workflow_conf);
	db_insert('node_workflow_item')
		->fields(array(
			'wid' => $form_state['values']['wid'],
			'data' => serialize($workflow_conf),
		))->execute();
}

function node_workflow_step_form($form, &$form_state, $node_type, $step = NULL) {
	$form['id'] = array(
		'#type' => 'hidden',
		'#value' => $step['id'],
	);

	$form['type'] = array(
		'#type' => 'hidden',
		'#value' => 'step'
	);

	$form['node_type'] = array(
		'#type' => 'hidden',
		'#value' => $node_type->type,
	);

	$db_query_wid = db_select('node_workflow', 'nw')
		->fields('nw')
		->condition('nw.node_type', $node_type->type, '=');
	$db_query_wid_result = $db_query_wid->execute()->fetchAssoc();

	$form['wid'] = array(
		'#type' => 'hidden',
		'#value' => $db_query_wid_result['wid'],
	);

	$form['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Step name'),
		'#size' => 60,
		'#maxlength' => 128,
		'#required' => TRUE,
		'#default_value' => $step['name'],
	);

	$form['machine_name'] = array(
		'#type' => 'machine_name',
		'#maxlength' => 21,
		'#machine_name' => array(
			'source' => array('name'),
			'exists' => 'step_name_exists_check',
		),
	);

	$form['description'] = array(
		'#title' => t('Description'),
		'#type' => 'textarea',
		'#default_value' => $step['description'],
	);

	$options = array();
	if (!empty($step)) {
		$options = unserialize($step['options']);
	}

	$form['conditional_check_func'] = array(
		'#title' => t('Conditional function'),
		'#type' => 'textfield',
		'#default_value' => empty($options['conditional_check_func']) ?'node_workflow_conditional_func_default' : $options['conditional_check_func'],
	);

	$field_info_instances = field_info_instances('node', $node_type->type);
//	dpm($field_info_instances);
	$fields_options = array();

	$field_default_value = array();
	$field_default_value_required = array();
	foreach ($field_info_instances as $field_info_instance) {
		$fields_options[$field_info_instance['field_name']] = $field_info_instance['label'];
		if ($field_info_instance['required']) {
			$field_default_value_required[] = $field_info_instance['field_name'];
		}
	}

	$field_default_values = array();
	if (isset($options['field']['edit'])) {
		foreach ($options['field']['edit'] as $value) {
			if ($value) {
				$field_default_value[] = $value;
			}
		}
		$field_default_values = $field_default_value;
	} else {
		$field_default_values = $field_default_value_required;
	}

	$form['field'] = array(
		'#type' => 'fieldset',
		'#title' => t('Field settings'),
		'#weight' => 5,
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
		'#tree' => TRUE,
	);

	$form['field']['edit'] = array(
		'#type' => 'checkboxes',
		'#options' => $fields_options,
		'#default_value' => $field_default_values,
		'#title' => t('Please select which fields could be edited at this step?'),
	);

	$form['permission_purview'] = array(
		'#type' => 'checkbox',
		'#title' => t('Group'),
		'#default_value' => isset($options['permission_purview']) ?  $options['permission_purview']: 0,
	);

	$gids = og_get_all_group('node');
	if (count($gids)) {
		$form['permission'] = array(
			'#type' => 'fieldset',
			'#title' => t('Permission settings'),
			'#weight' => 5,
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
			'#tree' => TRUE,
		);

		foreach ($gids as $gid) {
			$gnode = node_load($gid);
			$form['permission'][$gid] = array(
				'#type' => 'fieldset',
				'#title' => $gnode->title,
				'#weight' => 5,
				'#collapsible' => TRUE,
				'#collapsed' => TRUE,
			);
			//Fixme: need improvements at node type configuration form
			$group_roles = og_roles('node', 'department', $gid);

			foreach ($group_roles as $rid => $rname) {
				$form['permission'][$gid][$rid] = array(
					'#type' => 'fieldset',
					'#title' => $rname,
					'#weight' => 5,
					'#collapsible' => TRUE,
					'#collapsed' => TRUE,
				);

				$settings = array();
				if(isset($options['permission'])) {
					foreach ($options['permission'][$gid][$rid]['settings'] as $value) {
						if ($value) {
							$settings[] = $value;
						}
					}
				}

				$form['permission'][$gid][$rid]['settings'] = array(
					'#type' => 'checkboxes',
					'#title' => t('Please set for each action type?'),
					'#options' => array('view' => t('View'), 'update' => t('update')),
					'#default_value' => $settings,
				);
			}

			/*
			$permission_view_default_value = array();
			if (isset($options['permission'][$gid]['view'])) {
				foreach ($options['permission'][$gid]['view'] as $value) {
					if ($value) {
						$permission_view_default_value[] = $value;
					}
				}
			}

			$form['permission'][$gid]['view'] = array(
				'#type' => 'checkboxes',
				'#title' => t('Please select which roles could view this content at this step?'),
				'#options' => $group_roles,
				'#default_value' => $permission_view_default_value,
			);


			$permission_edit_default_value = array();
			if (isset($options['permission'][$gid]['edit'])) {
				foreach ($options['permission'][$gid]['edit'] as $value) {
					if ($value) {
						$permission_edit_default_value[] = $value;
					}
				}
			}

			$form['permission'][$gid]['edit'] = array(
				'#type' => 'checkboxes',
				'#title' => t('Please select which roles could edit this content at this step?'),
				'#options' => $group_roles,
				'#default_value' => $permission_edit_default_value,
			);
			*/

		}
	}

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#weight' => 6,
	);

	return $form;
}

function node_workflow_step_form_validate($form, &$form_state) {
	$conditional_check_func = $form_state['values']['conditional_check_func'];
	if (!function_exists($conditional_check_func)) {
		form_set_error('conditional_check_func', t('Conditional check function not existing.'));
	}
}

/**
 * Submit handler for node_workflow_step_add form
 *
 * @param $form
 * @param $form_state
 *
 * @throws \Exception
 */
function node_workflow_step_form_submit($form, &$form_state) {
	form_state_values_clean($form_state);
	if (isset($form_state['values']['id'])) {
		db_update('node_workflow_row')
			->fields(array(
				'wid' => $form_state['values']['wid'],
				'name' => $form_state['values']['name'],
				'type' => $form_state['values']['type'],
				'description' => $form_state['values']['description'],
				'machine_name' => $form_state['values']['machine_name'],
				'options' => serialize($form_state['values']),
			))->condition('id', $form_state['values']['id'])->execute();
	}
	else {
		$id = db_insert('node_workflow_row')
			->fields(array(
				'wid' => $form_state['values']['wid'],
				'name' => $form_state['values']['name'],
				'type' => $form_state['values']['type'],
				'description' => $form_state['values']['description'],
				'machine_name' => $form_state['values']['machine_name'],
				'options' => serialize($form_state['values']),
			))->execute();
		//todo: need optimized. it's not nessccary to write database twice.
		$form_state['values']['id'] = $id;
		db_update('node_workflow_row')
			->fields(array(
					'options' => serialize($form_state['values']),
				))->condition('id', $id, '=')->execute();
	}
	$form_state['redirect'][] = 'admin/structure/types/manage/' . $form_state['values']['node_type'] . '/node_workflow';
}

function node_workflow_step_delete_confirm_form($form, &$form_state, $node_type, $step) {
	$form['node_type'] = array(
		'#type' => 'hidden',
		'#value' => $node_type->type,
	);

	$form['step'] = array(
		'#type' => 'hidden',
		'#value' => $step['id'],
	);

	$form['actions'] = array('#type' => 'actions');
	$form['actions']['delete'] = array(
		'#type' => 'button',
		'#value' => t('Delete'),
		'#executes_submit_callback' => TRUE,
	);
	$form['actions']['cancel'] = array(
		'#markup' => l(t('Cancel'), 'admin/structure/types/manage/' . $node_type->type . '/node_workflow'),
	);


	return $form;
}

function node_workflow_step_delete_confirm_form_submit($form, &$form_state) {
	$row_data = node_workflow_row_get_data($form_state['values']['node_type'], $form_state['values']['step']);
	$ids = array_keys($row_data);
	$ids[] = $form_state['values']['step'];

	db_delete('node_workflow_row')->condition('id', $ids)->execute();
	$form_state['redirect'][] = 'admin/structure/types/manage/' . $form_state['values']['node_type'] . '/node_workflow';
}


function node_workflow_initial_step_form($form, &$form_state, $node_type, $initial_step = NULL) {
	$form['id'] = array(
		'#type' => 'hidden',
		'#value' => $initial_step['id'],
	);

	$form['type'] = array(
		'#type' => 'hidden',
		'#value' => 'initial_step'
	);

	$form['node_type'] = array(
		'#type' => 'hidden',
		'#value' => $node_type->type,
	);

	$db_query_wid = db_select('node_workflow', 'nw')
		->fields('nw')
		->condition('nw.node_type', $node_type->type, '=');
	$db_query_wid_result = $db_query_wid->execute()->fetchAssoc();

	$form['wid'] = array(
		'#type' => 'hidden',
		'#value' => $db_query_wid_result['wid'],
	);

	$form['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Step name'),
		'#size' => 60,
		'#maxlength' => 128,
		'#required' => TRUE,
		'#default_value' => $initial_step['name'],
	);

	$form['machine_name'] = array(
		'#type' => 'machine_name',
		'#maxlength' => 21,
		'#machine_name' => array(
			'source' => array('name'),
			'exists' => 'step_name_exists_check',
		),
	);

	$form['description'] = array(
		'#title' => t('Description'),
		'#type' => 'textarea',
		'#default_value' => $initial_step['description'],
	);

	$options = array();
	if (!empty($initial_step)) {
		$options = unserialize($initial_step['options']);
	}

	$field_info_instances = field_info_instances('node', $node_type->type);
	$fields_options = array();

	$field_default_value = array();
	$field_default_value_required = array();
	foreach ($field_info_instances as $field_info_instance) {
		$fields_options[$field_info_instance['field_name']] = $field_info_instance['label'];
		if ($field_info_instance['required']) {
			$field_default_value_required[] = $field_info_instance['field_name'];
		}
	}

	if (isset($options['field']['edit'])) {
		foreach ($options['field']['edit'] as $value) {
			if ($value) {
				$field_default_value[] = $value;
			}
		}
	}

	$form['field'] = array(
		'#type' => 'fieldset',
		'#title' => t('Field settings'),
		'#weight' => 5,
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#tree' => TRUE,
	);

	$form['field']['edit'] = array(
		'#type' => 'checkboxes',
		'#options' => $fields_options,
		'#default_value' => array_merge($field_default_value, $field_default_value_required),
		'#title' => t('Please select which fields could be edited at this step?'),
	);

	$gids = og_get_all_group('node');
	if (count($gids)) {
		$form['permission'] = array(
			'#type' => 'fieldset',
			'#title' => t('Permission settings'),
			'#weight' => 5,
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
			'#tree' => TRUE,
		);

		foreach ($gids as $gid) {
			$gnode = node_load($gid);
			$form['permission'][$gid] = array(
				'#type' => 'fieldset',
				'#title' => $gnode->title,
				'#weight' => 5,
				'#collapsible' => TRUE,
				'#collapsed' => TRUE,
			);
			//Fixme: need improvements at node type configuration form
			$group_roles = og_roles('node', 'department', $gid);

			foreach ($group_roles as $rid => $rname) {
				$form['permission'][$gid][$rid] = array(
					'#type' => 'fieldset',
					'#title' => $rname,
					'#weight' => 5,
					'#collapsible' => TRUE,
					'#collapsed' => TRUE,
				);

				$settings = array();
				if(isset($options['permission'])) {
					foreach ($options['permission'][$gid][$rid]['settings'] as $value) {
						if ($value) {
							$settings[] = $value;
						}
					}
				}

				$form['permission'][$gid][$rid]['settings'] = array(
					'#type' => 'checkboxes',
					'#title' => t('Please set for each action type?'),
					'#options' => array('create' => t('create')),
					'#default_value' => $settings,
				);
			}
		}
	}

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#weight' => 6,
	);

	return $form;
}

/**
 * Submit handler for node_workflow_initial_step_add form
 *
 * @param $form
 * @param $form_state
 *
 * @throws \Exception
 */
function node_workflow_initial_step_form_submit($form, &$form_state) {
	form_state_values_clean($form_state);
	if (isset($form_state['values']['id'])) {
		db_update('node_workflow_row')
			->fields(array(
				'wid' => $form_state['values']['wid'],
				'name' => $form_state['values']['name'],
				'type' => $form_state['values']['type'],
				'description' => $form_state['values']['description'],
				'machine_name' => $form_state['values']['machine_name'],
				'options' => serialize($form_state['values']),
			))->condition('id', $form_state['values']['id'])->execute();
	}
	else {
		$id = db_insert('node_workflow_row')
			->fields(array(
				'wid' => $form_state['values']['wid'],
				'name' => $form_state['values']['name'],
				'type' => $form_state['values']['type'],
				'description' => $form_state['values']['description'],
				'machine_name' => $form_state['values']['machine_name'],
				'options' => serialize($form_state['values']),
			))->execute();
		//todo: need optimized. it's not nessccary to write database twice.
		$form_state['values']['id'] = $id;
		db_update('node_workflow_row')
			->fields(array(
				'options' => serialize($form_state['values']),
			))->condition('id', $id, '=')->execute();
	}
	$form_state['redirect'][] = 'admin/structure/types/manage/' . $form_state['values']['node_type'] . '/node_workflow';
}


function node_workflow_ending_step_form($form, &$form_state, $node_type, $ending_step = NULL) {
	$form['id'] = array(
		'#type' => 'hidden',
		'#value' => $ending_step['id'],
	);

	$form['type'] = array(
		'#type' => 'hidden',
		'#value' => 'ending_step'
	);

	$form['node_type'] = array(
		'#type' => 'hidden',
		'#value' => $node_type->type,
	);

	$db_query_wid = db_select('node_workflow', 'nw')
		->fields('nw')
		->condition('nw.node_type', $node_type->type, '=');
	$db_query_wid_result = $db_query_wid->execute()->fetchAssoc();

	$form['wid'] = array(
		'#type' => 'hidden',
		'#value' => $db_query_wid_result['wid'],
	);

	$form['guide'] = array(
		'#markup' => t('In this step, now you only set which role could view this node at this step.'),
	);

	$form['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Step name'),
		'#size' => 60,
		'#maxlength' => 128,
		'#required' => TRUE,
		'#default_value' => $ending_step['name'],
	);

	$form['machine_name'] = array(
		'#type' => 'machine_name',
		'#maxlength' => 21,
		'#machine_name' => array(
			'source' => array('name'),
			'exists' => 'step_name_exists_check',
		),
	);

	$form['description'] = array(
		'#title' => t('Description'),
		'#type' => 'textarea',
		'#default_value' => $ending_step['description'],
	);

	$options = array();
	if (!empty($ending_step)) {
		$options = unserialize($ending_step['options']);
	}


	$form['permission_purview'] = array(
		'#type' => 'checkbox',
		'#title' => t('Group'),
		'#default_value' => isset($options['permission_purview']) ?  $options['permission_purview']: 0,
	);

	$gids = og_get_all_group('node');
	if (count($gids)) {
		$form['permission'] = array(
			'#type' => 'fieldset',
			'#title' => t('Permission settings'),
			'#weight' => 5,
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
			'#tree' => TRUE,
		);

		foreach ($gids as $gid) {
			$gnode = node_load($gid);
			$form['permission'][$gid] = array(
				'#type' => 'fieldset',
				'#title' => $gnode->title,
				'#weight' => 5,
				'#collapsible' => TRUE,
				'#collapsed' => TRUE,
			);
			//Fixme: need improvements at node type configuration form
			$group_roles = og_roles('node', 'department', $gid);

			foreach ($group_roles as $rid => $rname) {
				$form['permission'][$gid][$rid] = array(
					'#type' => 'fieldset',
					'#title' => $rname,
					'#weight' => 5,
					'#collapsible' => true,
					'#collapsed' => true,
				);

				$settings = array();
				if(isset($options['permission'])) {
					foreach ($options['permission'][$gid][$rid]['settings'] as $value) {
						if ($value) {
							$settings[] = $value;
						}
					}
				}

				$form['permission'][$gid][$rid]['settings'] = array(
					'#type' => 'checkboxes',
					'#title' => t('Please set for each action type?'),
					'#options' => array('view' => t('View')),
					'#default_value' => $settings,
				);
			}
		}
	}

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#weight' => 6,
	);

	return $form;
}

/**
 * Submit handler for node_workflow_ending_step_add form
 *
 * @param $form
 * @param $form_state
 *
 * @throws \Exception
 */
function node_workflow_ending_step_form_submit($form, &$form_state) {
	form_state_values_clean($form_state);
	if (isset($form_state['values']['id'])) {
		db_update('node_workflow_row')
			->fields(array(
				'wid' => $form_state['values']['wid'],
				'name' => $form_state['values']['name'],
				'type' => $form_state['values']['type'],
				'description' => $form_state['values']['description'],
				'machine_name' => $form_state['values']['machine_name'],
				'options' => serialize($form_state['values']),
			))->condition('id', $form_state['values']['id'])->execute();
	}
	else {
		$id = db_insert('node_workflow_row')
			->fields(array(
				'wid' => $form_state['values']['wid'],
				'name' => $form_state['values']['name'],
				'type' => $form_state['values']['type'],
				'description' => $form_state['values']['description'],
				'machine_name' => $form_state['values']['machine_name'],
				'options' => serialize($form_state['values']),
			))->execute();
		//todo: need optimized. it's not nessccary to write database twice.
		$form_state['values']['id'] = $id;
		db_update('node_workflow_row')
			->fields(array(
				'options' => serialize($form_state['values']),
			))->condition('id', $id, '=')->execute();
	}
	$form_state['redirect'][] = 'admin/structure/types/manage/' . $form_state['values']['node_type'] . '/node_workflow';
}

function node_workflow_ending_step_delete_confirm_form($form, &$form_state, $node_type, $ending_step) {
	$form['node_type'] = array(
		'#type' => 'hidden',
		'#value' => $node_type->type,
	);

	$form['ending_step'] = array(
		'#type' => 'hidden',
		'#value' => $ending_step['id'],
	);

	$form['actions'] = array('#type' => 'actions');
	$form['actions']['delete'] = array(
		'#type' => 'button',
		'#value' => t('Delete'),
		'#executes_submit_callback' => TRUE,
	);
	$form['actions']['cancel'] = array(
		'#markup' => l(t('Cancel'), 'admin/structure/types/manage/' . $node_type->type . '/node_workflow'),
	);

	return $form;
}

function node_workflow_ending_step_delete_confirm_form_submit($form, &$form_state) {
//	$row_data = node_workflow_row_get_data($form_state['values']['node_type'], $form_state['values']['step']);
	$ids = array();
	$ids[] = $form_state['values']['ending_step'];

	db_delete('node_workflow_row')->condition('id', $ids)->execute();
	$form_state['redirect'][] = 'admin/structure/types/manage/' . $form_state['values']['node_type'] . '/node_workflow';
}

function node_workflow_action_form($form, &$form_state, $node_type, $action = NULL) {
	$form['id'] = array(
		'#type' => 'hidden',
		'#value' => isset($action['id']) ? $action['id'] : '',
	);
	$form['type'] = array(
		'#type' => 'hidden',
		'#value' => 'action'
	);
	$form['node_type'] = array(
		'#type' => 'hidden',
		'#value' => $node_type->type,
	);

	$db_query_wid = db_select('node_workflow', 'nw')
		->fields('nw')
		->condition('nw.node_type', $node_type->type, '=');
	$db_query_wid_result = $db_query_wid->execute()->fetchAssoc();
	$form['wid'] = array(
		'#type' => 'hidden',
		'#value' => $db_query_wid_result['wid'],
	);

	$form['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Action name'),
		'#size' => 60,
		'#maxlength' => 128,
		'#required' => TRUE,
		'#default_value' => isset($action['name']) ? $action['name'] : '',
	);

	$form['machine_name'] = array(
		'#type' => 'machine_name',
		'#maxlength' => 21,
		'#machine_name' => array(
			'source' => array('name'),
			'exists' => 'action_name_exists_check',
		),
	);

	$form['description'] = array(
		'#title' => t('Description'),
		'#type' => 'textarea',
		'#default_value' => isset($action['description']) ? $action['description'] : '',
	);

	$options = unserialize($action['options']);
	/*
	$form['action_condition_func'] = array(
		'#type' => 'textfield',
		'#title' => t('Condition of action check function'),
		'#size' => 60,
		'#maxlength' => 128,
		'#required' => TRUE,
		'#description' => t('Please enter an existing function in this field.'),
		'#default_value' => $options['action_condition_func'],
	);
	*/
	$form['action_button'] = array(
		'#type' => 'textfield',
		'#title' => T('Button display name'),
		'#size' => 60,
		'#required' => TRUE,
		'#default_value' => isset($options['action_button']) ? $options['action_button'] : '',
	);

	$form['action_func'] = array(
		'#type' => 'textfield',
		'#title' => t('Action function'),
		'#size' => 60,
		'#maxlength' => 128,
		'#required' => TRUE,
		'#description' => t('Please enter an existing function in this field.'),
		'#default_value' => isset($options['action_func']) ? $options['action_func'] : '',
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#weight' => 6,
	);
	return $form;
}

/**
 * validate handler for node workflow action form.
 *
 * @param $form
 * @param $form_state
 */
function node_workflow_action_form_validate($form, &$form_state) {
//	$action_condition_func = $form_state['values']['action_condition_func'];
//	if (!function_exists($action_condition_func)) {
//		form_set_error('action_condition_func', t('Condition function not existing.'));
//	}

	$action_func = $form_state['values']['action_func'];
	if (!function_exists($action_func)) {
		form_set_error('action_func', t('Action function not existing.'));
	}
}

/**
 * Submit handler for node workflow action form.
 *
 * @param $form
 * @param $form_state
 *
 * @throws \Exception
 */
function node_workflow_action_form_submit($form, &$form_state) {
	form_state_values_clean($form_state);
	if (isset($form_state['values']['id']) && !empty($form_state['values']['id'])) {
		db_update('node_workflow_row')
			->fields(array(
				'wid' => $form_state['values']['wid'],
				'name' => $form_state['values']['name'],
				'type' => $form_state['values']['type'],
				'description' => $form_state['values']['description'],
				'machine_name' => $form_state['values']['machine_name'],
				'options' => serialize(form_state_values_clean($form_state['values'])),
				'options' => serialize($form_state['values']),
			))->condition('id', $form_state['values']['id'])->execute();
	}
	else {
		$id = db_insert('node_workflow_row')
			->fields(array(
				'wid' => $form_state['values']['wid'],
				'name' => $form_state['values']['name'],
				'type' => $form_state['values']['type'],
				'description' => $form_state['values']['description'],
				'machine_name' => $form_state['values']['machine_name'],
				'options' => serialize($form_state['values']),
			))->execute();
		//todo: need optimized. it's not nessccary to write database twice.
		$form_state['values']['id'] = $id;
		db_update('node_workflow_row')
			->fields(array(
				'options' => serialize($form_state['values']),
			))->condition('id', $id, '=')->execute();
	}

	$form_state['redirect'][] = 'admin/structure/types/manage/' . $form_state['values']['node_type'] . '/node_workflow';
}

/**
 * Node workflow action delete form
 *
 * @param $form
 * @param $form_state
 * @param $node_type
 * @param $action
 *
 * @return mixed
 */
function node_workflow_action_delete_confirm_form($form, &$form_state, $node_type, $action) {
	$form['node_type'] = array(
		'#type' => 'hidden',
		'#value' => $node_type->type,
	);

	$form['action'] = array(
		'#type' => 'hidden',
		'#value' => $action['id'],
	);

	$form['actions'] = array('#type' => 'actions');
	$form['actions']['delete'] = array(
		'#type' => 'button',
		'#value' => t('Delete'),
		'#executes_submit_callback' => TRUE,
	);
	$form['actions']['cancel'] = array(
		'#markup' => l(t('Cancel'), 'admin/structure/types/manage/' . $node_type->type . '/node_workflow'),
	);

	return $form;
}

/**
 * Submit handler for node workflow action delete form
 *
 * @param $form
 * @param $form_state
 */
function node_workflow_action_delete_confirm_form_submit($form, &$form_state) {
	$row_data = node_workflow_row_get_data($form_state['values']['node_type'], $form_state['values']['action']);
	$ids = array_keys($row_data);
	$ids[] = $form_state['values']['action'];

	db_delete('node_workflow_row')->condition('id', $ids)->execute();
	$form_state['redirect'][] = 'admin/structure/types/manage/' . $form_state['values']['node_type'] . '/node_workflow';
}

function node_workflow_history($node) {
	return '1';
}

/**
 * Check handler for condition machine name.
 * @return bool
 */
function action_name_exists_check($condition_name) {
	$node_workflow_action_query = db_select('node_workflow_row', 'nwr')
		->fields('nwr')
		->condition('nwr.machine_name', $condition_name, '=')
		->condition('nwr.type', 'condtion', '=');
	$node_workflow_action_count = $node_workflow_action_query->execute()
		->rowCount();
	if ($node_workflow_action_count) {
		return TRUE;
	}

	return FALSE;
}

/**
 * Check handler for step machine name.
 * @return bool
 */
function step_name_exists_check($step_name, $element, $form_state) {
	if (isset($form_state['values']['id'])) {
		return FALSE;
	}
	$node_workflow_step_query = db_select('node_workflow_row', 'nwr')
		->fields('nwr')
		->condition('nwr.machine_name', $step_name, '=')
		->condition('nwr.type', 'step', '=');
	$node_workflow_step_count = $node_workflow_step_query->execute()->rowCount();
	if ($node_workflow_step_count) {
		return TRUE;
	}

	return FALSE;
}