<?php

/**
 * Implements hook_field_extra_fields().
 */
function node_workflow_field_extra_fields() {
	$extra = array();
	$entity_info = entity_get_info('node');
	foreach (array_keys($entity_info['bundles']) as $bundle) {
		$query_node_workflow = db_select('node_workflow', 'nw')
			->fields('nw')
			->condition('nw.node_type', $bundle, '=');

		$result_node_workflow = $query_node_workflow->execute()->fetchAll();
		$workflow_info = empty($result_node_workflow) ? array() : $result_node_workflow[0];

		if (isset($workflow_info->status) && $workflow_info->status) {
			$extra['node'][$bundle]['form']['node_workflow'] = array(
				'label' => t('Node Workflow'),
				'description' => t('Node workflow form elements.'),
				'weight' => 40,
			);
			$extra['node'][$bundle]['display']['node_workflow'] = array(
				'label' => t('Node Workflow'),
				'description' => t('Node workflow form elements.'),
				'weight' => 40,
			);
		}
	}

	return $extra;
}




